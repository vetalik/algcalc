angular.module('SalaryApp', [])
    .controller('SalaryController', function($scope) {
        $scope.title = "Salary calculator";

        $scope.calcSocial = function(bruto, percent) {
            if (typeof(percent) === "undefined") {
                percent = 0.105;
            }
            return bruto * percent;
        };

        $scope.calcIIN = function(bruto, social, percent, minimal) {
            if (typeof(percent) === "undefined") {
                percent = 0.23;
            }
            if (typeof(minimal) === "undefined") {
                minimal = 75;
            }
            return (bruto - social - minimal) * percent;
        };

        $scope.calcNeto = function(bruto) {
            var social = $scope.calcSocial(bruto);
            return bruto - social - $scope.calcIIN(bruto, social);
        };

        $scope.calculateNeto = function() {
            $scope.neto = $scope.calcNeto($scope.bruto)
        };
    });
