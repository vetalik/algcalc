#Algu kalkulators.

Uzdevumu jāuztaisa pa soliem (no 1. līdz 5.)

1. Jāuztaisa vienkāršoto web formu kas rēķina neto algu no bruto. Loģiku jāuzraksta izmantojot javascript.
    Aprēķināšanas formula:
        social = bruto * 10.05%
        IIN = (bruto - social - 75) * 23%
        neto = bruto - social - IIN

2. Jāuztaisa tādu pašu formu bet loģikai jāatrodas server sidā - java un ar ajax.

3. Pievienot atvieglojumi par apgādājamo personu. Jābūt iespējai ievadīt personas daudzums. Atvieglojums ir 165 par katru personu apgādībā.

4. Pievienot darba devēja izmaksas kopā.

5. Parēķināt algu uz citu valūtu (piem. Polijas zlots) izmantojot online servisu (piemēram XML no http://www.ecb.europa.eu/stats/exchange/eurofxref/html/index.en.html )..

Rezultātā jābūt war fails kas būtu iespējams palaist jebkurā tomcat7 serverī.
