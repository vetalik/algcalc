angular.module('SalaryApp', ['ngResource'])
    .controller('SalaryController', function($scope, SalaryService, CurrenciesService) {
        $scope.title = "Salary calculator";

        $scope.calcSocial = function(bruto, percent) {
            if (typeof(percent) === "undefined") {
                percent = 0.105;
            }
            return bruto * percent;
        };

        $scope.calcMinimal = function(incentives) {
            var minimal = 75;
            if (!incentives){
                incentives = 0;
            }
            var per_person = 165;
            return minimal + per_person * incentives;
        }

        $scope.calcIIN = function(bruto, social, percent, minimal) {
            if (!percent) {
                percent = 0.23;
            }
            if (!minimal) {
                minimal = 75;
            }
            return (bruto - social - minimal) * percent;
        };

        $scope.calcNeto = function(bruto, incentives) {
            var social = $scope.calcSocial(bruto);
            var minimal = $scope.calcMinimal(incentives);
            return bruto - social - $scope.calcIIN(bruto, social, null, minimal);
        };

        $scope.calculateNeto = function() {
            $scope.neto = $scope.calcNeto($scope.bruto, $scope.incentives);
        };

        CurrenciesService.get(function(data){
            $scope.currencies = _.clone(data);
            console.log($scope.currencies);
        });

        $scope.getNeto = function(){
            if (!$scope.incentives){
                $scope.incentives = 0;
            }
            if (!$scope.selectedCurrency){
                $scope.selectedCurrency = 0;
            }
            SalaryService.get(
                {
                    bruto: $scope.bruto,
                    incentives: $scope.incentives,
                    currency: $scope.selectedCurrency
                }, function(data) {
                $scope.serverNeto = data.neto;
                $scope.eployeeCosts = data.employee_costs;
            })
        };
    })
    .service('SalaryService', function($resource, $location){
        var url = $location.absUrl();
        return $resource(url+"calculate/social/:bruto/:incentives/:currency");
    })
    .service('CurrenciesService', function($resource, $location){
        var url = $location.absUrl();
        return $resource(url + "/currencies");
    });
