package com.solcalc;

import spark.*;
import spark.servlet.SparkApplication;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;
import static spark.SparkBase.staticFileLocation;

public class SolCalc implements SparkApplication {

    public static void main(String[] args) {

        SparkApplication app = new SolCalc();
        app.init();
    }
    public void init() {
        staticFileLocation("/public");

        get("/calculate/social/:bruto/:incentives/:currency", "application/json", (request, response) -> {
            Double bruto;
            Double neto;
            Double employeeCosts;
            String currency;
            Double rate = 0.0;
            Integer incentives;
            bruto = Double.parseDouble(request.params(":bruto"));
            incentives = Integer.parseInt(request.params(":incentives"));
            currency = request.params(":currency");
            Calculators calculator = new Calculators();
            Map<String, Double> rates = calculator.getRates();
            rate = rates.getOrDefault(currency, 1.0);
            neto = calculator.calcNeto(bruto, incentives);
            employeeCosts = calculator.calcEmployerCosts(bruto);
            Map<String, Double> res = new HashMap<>();
            res.put("neto", neto * rate);
            res.put("employee_costs", employeeCosts * rate);
            return res;
        }, new JsonTransformer());

        get("/currencies", "application/json", (request, response) -> {
            Calculators calculator = new Calculators();
            HashMap<String, String> currencies = calculator.getCurrenciesList();
            return currencies;
        }, new JsonTransformer());
    }
}
