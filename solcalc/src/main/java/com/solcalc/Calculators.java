package com.solcalc;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by vetalik on 4/18/15.
 */
public class Calculators {
    private Double calcSocial(Double brutoSalary){
        final Double percent = 0.105;
        return brutoSalary * percent;
    }

    public Double calcMinimal(Integer incentives) {
        final Double minimal = 75.0;
        final Double perPerson = 165.0;
        return minimal + perPerson * incentives;
    }

    private Double calcIIN (Double bruto, Double social, Double minimal) {
        final Double percent = 0.23;
        return (bruto - social - minimal) * percent;
    }

    public Double calcNeto (Double bruto, Integer incentives) {
        Double social = this.calcSocial(bruto);
        Double minimal = this.calcMinimal(incentives);
        return bruto - social - this.calcIIN(bruto, social, minimal);
    }

    public Double calcEmployerCosts (Double bruto){
        Double EmployeeRate = 0.2359;
        Double Risks = 0.36;
        return bruto + bruto * EmployeeRate + Risks;
    }

    public Document getCurrencies () throws MalformedURLException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        String url = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = db.parse(new URL(url).openStream());
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }


    public static HashMap<String, Double> getRates() throws Exception {
        HashMap<String, Double> res = new HashMap<String, Double>();
        String url = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        Document doc = factory.newDocumentBuilder().parse(new URL(url).openStream());
        NodeList nodes = doc.getElementsByTagName("Cube");
        Node n = nodes.item(1);
        NodeList children = n.getChildNodes();
        for (int i =1; i< children.getLength(); i++){
            Node p = children.item(i);

            NamedNodeMap nnm = p.getAttributes();
            try {
                res.put(nnm.getNamedItem("currency").getNodeValue(), Double.parseDouble(nnm.getNamedItem("rate").getNodeValue()));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            System.out.println(children.item(i));
        }
        return res;
    }

    public static HashMap<String, String> getCurrenciesList() throws Exception {
        HashMap<String, String> res = new HashMap<>();
        String url = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        Document doc = factory.newDocumentBuilder().parse(new URL(url).openStream());
        NodeList nodes = doc.getElementsByTagName("Cube");
        Node n = nodes.item(1);
        NodeList children = n.getChildNodes();
        for (int i = 1; i < children.getLength(); i++) {
            Node p = children.item(i);

            NamedNodeMap nnm = p.getAttributes();
            try {
                res.put(nnm.getNamedItem("currency").getNodeValue(), nnm.getNamedItem("currency").getNodeValue());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

}
